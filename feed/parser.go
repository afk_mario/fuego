package feed

import (
	"encoding/json"
	"os"

	"fuego/logger"
)

// Load Loads feed info from file
func (f *Data) Load() {
	file, err := os.Open("feed.json")
	if err != nil {
		logger.Fatal(`[FEED] Failed loading feed file: %v`, err)
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&f)
	if err != nil {
		logger.Fatal(`[FEED] Cannot get configuration from file: %v`, err)
	}
	logger.Info(`[FEED] Feed loaded: %v`, f.Slug)
}
