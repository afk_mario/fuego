package feed

import (
	"time"
)

// Data is the holds the feed information
type Data struct {
	URL          string
	Slug         string
	Refresh      int64
	LastFetched  time.Time
	TimesFetched int64
	Content      []byte
}
