package feed

import (
	"io/ioutil"
	"net/http"
	"time"

	"fuego/logger"
)

// FeedData holds parsed Feed data
var FeedData *Data

// Fetch fetches the feed data feed content
func (f *Data) Fetch() (err error) {
	client := http.DefaultClient
	logger.Info(`[FEED] Fetching: %v`, f.Slug)

	req, err := http.NewRequest("GET", f.URL, nil)
	if err != nil {
		return err
	}
	req.Header.Set("User-Agent", "fuego/1.0")
	resp, err := client.Do(req)

	if err != nil {
		return err
	}

	if resp != nil {
		defer func() {
			ce := resp.Body.Close()
			if ce != nil {
				err = ce
			}
		}()
	}

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
	}

	// TODO: Check if there is a way to simplify this
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	f.Content = data

	t := time.Now()
	f.LastFetched = t
	f.TimesFetched++

	logger.Info(`[FEED] Fetched at: %v`, f.LastFetched.Format("01/02/06"))
	logger.Info(`[FEED] times Fetched: %v`, f.TimesFetched)

	return err
}
