module fuego

go 1.12

require (
	github.com/gomarkdown/markdown v0.0.0-20190222000725-ee6a7931a1e4
	github.com/gorilla/mux v1.7.3
	github.com/microcosm-cc/bluemonday v1.0.2
	github.com/mmcdole/gofeed v1.0.0-beta2
	github.com/mmcdole/goxpp v0.0.0-20181012175147-0068e33feabf // indirect
	gitlab.com/afk_mario/fuego v0.0.0-20190625222714-3405acacceea
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
	golang.org/x/text v0.3.2 // indirect
	miniflux.app v0.0.0-20190713111139-48abf5721815
)
