package config

import (
	"fmt"
	"strings"
)

const (
	defaultLogDateTime  = false
	defaultListenAddr   = "127.0.0.1:8000"
	defaultReadTimeout  = 1000
	defaultWriteTimeout = 1000
	defaultStatic       = "static"
	defaultTemplates    = "templates"
	defaultDebug        = true
	defaultFeedFile     = "feed.json"
)

// Options contains configuration options.
type Options struct {
	debug        bool
	logDateTime  bool
	listenAddr   string
	static       string
	templates    string
	domain       string
	feedFile     string
	readTimeout  int
	writeTimeout int
}

// NewOptions returns Options with default values.
func NewOptions() *Options {
	return &Options{
		logDateTime:  defaultLogDateTime,
		listenAddr:   defaultListenAddr,
		readTimeout:  defaultReadTimeout,
		writeTimeout: defaultWriteTimeout,
		static:       defaultStatic,
		templates:    defaultTemplates,
		debug:        defaultDebug,
		feedFile:     defaultFeedFile,
	}
}

// LogDateTime returns true if the date/time should be displayed in log messages.
func (o *Options) LogDateTime() bool {
	return o.logDateTime
}

// HasDebugMode returns true if debug mode is enabled.
func (o *Options) HasDebugMode() bool {
	return o.debug
}

// ListenAddr returns the listen address for the HTTP server.
func (o *Options) ListenAddr() string {
	return o.listenAddr
}

// ReadTimeout returns the server port number.
func (o *Options) ReadTimeout() int {
	return o.readTimeout
}

// WriteTimeout returns the server port number.
func (o *Options) WriteTimeout() int {
	return o.writeTimeout
}

// StaticFolder returns the route to the static folder.
func (o *Options) StaticFolder() string {
	return o.static
}

// TemplatesFolder returns the route to the static folder.
func (o *Options) TemplatesFolder() string {
	return o.templates
}

// Domain returns the main domain.
func (o *Options) Domain() string {
	return o.domain
}

// FeedFile returns feed file path.
func (o *Options) FeedFile() string {
	return o.feedFile
}

func (o *Options) String() string {
	var builder strings.Builder
	builder.WriteString(fmt.Sprintf("DEBUG: %v\n", o.debug))
	builder.WriteString(fmt.Sprintf("LISTEN_ADDR: %v\n", o.listenAddr))
	builder.WriteString(fmt.Sprintf("READ_TIMEOUT: %v\n", o.readTimeout))
	builder.WriteString(fmt.Sprintf("WRITE_TIMEOUT: %v\n", o.writeTimeout))
	builder.WriteString(fmt.Sprintf("STATIC_FOLDER: %v\n", o.static))
	builder.WriteString(fmt.Sprintf("TEMPLATES_FOLDER: %v\n", o.templates))
	builder.WriteString(fmt.Sprintf("FEED_FILE: %v\n", o.feedFile))
	return builder.String()
}
