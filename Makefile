APP := fuego
DOCKER_IMAGE := afk_mcz/fuego
VERSION := $(shell git rev-parse --short HEAD)
BUILD_DATE := `date +%FT%T%z`
LD_FLAGS := "-s -w -X 'fuego.com/version.Version=$(VERSION)' -X 'fuego.com/version.BuildDate=$(BUILD_DATE)'"
PKG_LIST := $(shell go list ./... | grep -v /vendor/)
DB_URL := postgres://postgres:postgres@localhost/miniflux_test?sslmode=disable

export GO111MODULE=on

.PHONY: generate \
	fuego \
	linux-amd64 \
	linux-armv8 \
	linux-armv7 \
	linux-armv6 \
	linux-armv5 \
	linux-x86 \
	darwin-amd64 \
	freebsd-amd64 \
	freebsd-x86 \
	openbsd-amd64 \
	openbsd-x86 \
	netbsd-x86 \
	netbsd-amd64 \
	windows-amd64 \
	windows-x86 \
	build \
	run \
	clean \
	test \
	lint \
	integration-test \
	clean-integration-test \
	docker-images \
	docker-manifest

generate:
	@ go generate

fuego: generate
	@ go build -ldflags=$(LD_FLAGS) -o $(APP) main.go

build: fuego

run: generate
	@ go run main.go -debug

clean:
	@ rm -f $(APP)-* $(APP)

test:
	go test -cover -race -count=1 ./...

lint:
	@ golint -set_exit_status ${PKG_LIST}
