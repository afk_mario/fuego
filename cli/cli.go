package cli

import (
	"flag"
	"fmt"

	"fuego/config"
	"fuego/feed"
	"fuego/httpd"
	"fuego/logger"
	"fuego/version"
)

const (
	flagInfoHelp       = "Show application information"
	flagVersionHelp    = "Show application version"
	flagDebugModeHelp  = "Show debug logs"
	flagConfigFileHelp = "Load configuration file"
	flagConfigDumpHelp = "Print parsed configuration values"
)

// Parse parses command line arguments.
func Parse() {
	var (
		err            error
		flagInfo       bool
		flagVersion    bool
		flagDebugMode  bool
		flagConfigFile string
		flagConfigDump bool
	)

	flag.BoolVar(&flagInfo, "info", false, flagInfoHelp)
	flag.BoolVar(&flagInfo, "i", false, flagInfoHelp)
	flag.BoolVar(&flagVersion, "version", false, flagVersionHelp)
	flag.BoolVar(&flagVersion, "v", false, flagVersionHelp)
	flag.BoolVar(&flagDebugMode, "debug", false, flagDebugModeHelp)
	flag.StringVar(&flagConfigFile, "config-file", "", flagConfigFileHelp)
	flag.StringVar(&flagConfigFile, "c", "", flagConfigFileHelp)
	flag.BoolVar(&flagConfigDump, "config-dump", false, flagConfigDumpHelp)
	flag.Parse()

	cfg := config.NewParser()

	if flagConfigFile != "" {
		config.Opts, err = cfg.ParseFile(flagConfigFile)
		if err != nil {
			logger.Fatal("[CLI] %v", err)
		}
	}

	config.Opts, err = cfg.ParseEnvironmentVariables()
	if err != nil {
		logger.Fatal("[CLI] %v", err)
	}

	if flagConfigDump {
		fmt.Print(config.Opts)
		return
	}

	if config.Opts.LogDateTime() {
		logger.EnableDateTime()
	}

	if flagDebugMode || config.Opts.HasDebugMode() {
		logger.EnableDebug()
	}

	if flagInfo {
		info()
		return
	}

	if flagVersion {
		fmt.Println(version.Version)
		return
	}

	feed.FeedData = &feed.Data{}
	feed.FeedData.Load()
	feed.FeedData.Fetch()
	httpd.Serve()
}
