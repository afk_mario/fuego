package ui

import (
	"net/http"

	"fuego/config"
	"fuego/logger"
	"fuego/views"
)

func handleHome() http.HandlerFunc {
	templates := config.Opts.TemplatesFolder()
	homeView, err := views.NewView(
		templates,
		"base.html",
		"views/main/home.html",
	)

	if err != nil {
		logger.Fatal(`[UI] Failed to create home View: %v`, err)
	}

	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		views.Must(homeView.Render(w, nil))
	}
}
