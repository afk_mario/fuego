package ui

import (
	"net/http"

	"github.com/gorilla/mux"

	"fuego/config"
	"fuego/feed"
	"fuego/logger"
	"fuego/views"
)

func handleFeedInfo() http.HandlerFunc {
	templates := config.Opts.TemplatesFolder()

	infoView, err := views.NewView(
		templates,
		"base.html",
		"/views/main/info.html",
	)

	if err != nil {
		logger.Fatal(`[UI] Failed to create FeedInfo view: %v`, err)
	}

	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		vars := mux.Vars(r)
		slug := vars["feed-slug"]

		if slug != feed.FeedData.Slug {
			logger.Error(`[UI] Feed slug doesn't match feed: %v`)
			views.RedirectToNotFound(w, r, r.URL.Path)
		}
		views.Must(infoView.Render(w, feed.FeedData))
	}
}
