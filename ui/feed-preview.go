package ui

import (
	"net/http"
	"net/url"

	"github.com/mmcdole/gofeed"

	"fuego/config"
	"fuego/logger"
	"fuego/views"
)

func handleFeedPreviewPOST() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		rawURL := r.PostFormValue("url")
		encodedURL := url.PathEscape(rawURL)
		logger.Info(`[UI] feed preview url: %v`, rawURL)
		http.Redirect(w, r, "/p?url="+encodedURL, http.StatusSeeOther)
	}
}

func handleFeedPreview() http.HandlerFunc {
	templates := config.Opts.TemplatesFolder()
	feedPreviewView, err := views.NewView(
		templates,
		"base.html",
		"/views/feed/feed-parsed.html",
	)

	if err != nil {
		logger.Fatal(`[UI] Failed to create feedPreview View: %v`, err)
	}

	return func(w http.ResponseWriter, r *http.Request) {
		v := r.URL.Query()
		encodedURL := v.Get("url")
		decodedURL, err := url.PathUnescape(encodedURL)

		if err != nil {
			logger.Error(`[UI] Invalid feed URL on feed preview`)
			views.RedirectToError(w, r, "Invalid feed URL")
		}

		fp := gofeed.NewParser()
		context, err := fp.ParseURL(decodedURL)

		if err != nil {
			logger.Error(`[UI] Invalid feed URL on feed preview`)
			views.RedirectToError(w, r, "Invalid feed URL")
		}

		views.Must(feedPreviewView.Render(w, context))
	}
}
