package ui

import (
	"github.com/gorilla/mux"
	"net/http"

	"fuego/config"
	"fuego/views"
)

// Serve declares all routes for the user interface.
func Serve(router *mux.Router) {
	staticPath := "/" + config.Opts.StaticFolder() + "/"

	fs := http.FileServer(http.Dir("ui/" + staticPath))
	router.PathPrefix(staticPath).Handler(http.StripPrefix(staticPath, fs))

	uiRouter := router.NewRoute().Subrouter()
	uiRouter.HandleFunc("/", handleHome())
	uiRouter.HandleFunc("/err", handleError())

	uiRouter.HandleFunc("/p", handleFeedPreview()).Methods("GET").Queries("url", "{url}")
	uiRouter.HandleFunc("/p", views.RedirectToIndex).Methods("GET")
	uiRouter.HandleFunc("/p", handleFeedPreviewPOST()).Methods("POST")

	uiRouter.HandleFunc("/i/{feed-slug}", handleFeedInfo())

	uiRouter.HandleFunc("/f/{feed-slug}", views.RedirectToIndex).Methods("GET")
	uiRouter.HandleFunc("/f/{feed-slug}", feedFetchPost).Methods("POST")

	uiRouter.HandleFunc("/v/{feed-slug}", feedCached())

	uiRouter.NotFoundHandler = http.HandlerFunc(handleNotFound())
}
