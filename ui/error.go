package ui

import (
	"net/http"

	"fuego/config"
	"fuego/logger"
	"fuego/views"
)

// GET /err?msg=
// shows the error message page
func handleError() http.HandlerFunc {
	templates := config.Opts.TemplatesFolder()

	errorView, err := views.NewView(
		templates,
		"base.html",
		"/views/main/error.html",
	)

	if err != nil {
		logger.Fatal(`[UI] Failed to create error View: %v`, err)
	}

	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		vals := r.URL.Query()
		context := vals.Get("msg")
		views.Must(errorView.Render(w, context))
	}
}
