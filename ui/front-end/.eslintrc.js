module.exports = {
  globals: {
    STATIC_PATH: true,
  },
  parser: 'babel-eslint',
  extends: 'airbnb-base',
  plugins: ['prettier'],
  env: {
    browser: true,
    es6: true,
  },
  rules: {
    'no-console': 'off',
  },
};
