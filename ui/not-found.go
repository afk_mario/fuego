package ui

import (
	"fuego/config"
	"fuego/logger"
	"fuego/views"
	"net/http"
	"net/url"
)

func handleNotFound() http.HandlerFunc {
	templates := config.Opts.TemplatesFolder()

	notFoundView, err := views.NewView(
		templates,
		"base.html",
		"/views/main/404.html",
	)

	if err != nil {
		logger.Fatal(`[UI] Failed to create NotFound View: %v`, err)
	}

	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusNotFound)

		// TODO: if there is no url param
		// use the current url as message
		v := r.URL.Query()
		encodedURL := v.Get("url")
		decodedURL, err := url.PathUnescape(encodedURL)

		if err != nil {
			logger.Fatal(`[UI] Failed to parse message: %v`, err)
		}

		views.Must(notFoundView.Render(w, decodedURL))
	}
}
