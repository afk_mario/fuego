package ui

import (
	"net/http"

	"github.com/gorilla/mux"

	"fuego/feed"
	"fuego/logger"
	"fuego/views"
)

func feedFetchPost(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	slug := vars["feed-slug"]

	if slug != feed.FeedData.Slug {
		views.RedirectToNotFound(w, r, r.URL.Path)
	}

	if feed.FeedData.Fetch() != nil {
		logger.Error(`[UI] Unable to Fetch Feed: %v`)
		views.RedirectToError(w, r, "Error fething feed")
	}
	http.Redirect(w, r, "/i/"+feed.FeedData.Slug, http.StatusSeeOther)
}
