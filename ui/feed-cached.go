package ui

import (
	"bytes"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/mmcdole/gofeed"

	"fuego/config"
	"fuego/feed"
	"fuego/logger"
	"fuego/views"
)

func feedCached() http.HandlerFunc {
	templates := config.Opts.TemplatesFolder()
	feedCachedView, err := views.NewView(
		templates,
		"base.html",
		"/views/feed/feed-parsed.html",
	)

	if err != nil {
		logger.Fatal(`[UI] Failed to create feed cached View: %v`, err)
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var err error
		vars := mux.Vars(r)
		slug := vars["feed-slug"]

		if slug != feed.FeedData.Slug {
			views.RedirectToNotFound(w, r, r.URL.Path)
		}

		if feed.FeedData.Content == nil {
			err = feed.FeedData.Fetch()
		}

		if err != nil {
			logger.Error(`[UI] Unable to Fetch Feed: %v`)
			views.RedirectToError(w, r, "Error fething feed")
		}

		fp := gofeed.NewParser()
		data := bytes.NewReader(feed.FeedData.Content)
		context, err := fp.Parse(data)

		if err != nil {
			logger.Error(`[UI] Invalid Feed: %v`)
			views.RedirectToError(w, r, "Invalid Feed")
		}

		views.Must(feedCachedView.Render(w, context))
	}
}
