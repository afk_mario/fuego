FROM golang
LABEL maintainer="afk@ellugar.co"
ARG SRC_PATH=/go/src/gitlab.com/afk_mario/fuego

RUN mkdir -p $SRC_PATH
WORKDIR $SRC_PATH

COPY . ./

RUN go get github.com/gomarkdown/markdown
RUN go get github.com/microcosm-cc/bluemonday
RUN go get github.com/gorilla/mux
RUN go get github.com/mmcdole/gofeed

RUN go install gitlab.com/afk_mario/fuego

ENTRYPOINT /go/bin/fuego

EXPOSE 8000
