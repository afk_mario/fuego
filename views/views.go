package views

import (
	"fmt"
	"html/template"
	"net/http"
	"path/filepath"
)

// View is a struct that holds a template reference
type View struct {
	Template *template.Template
	Dir      string
	Layout   string
}

var (
	// LayoutDir Path for all the layout templates
	LayoutDir = "/layouts/"
	// TemplateExt extension of out templates
	TemplateExt = ".html"
)

// Render calls the execute method of the view template
func (view *View) Render(w http.ResponseWriter, data interface{}) error {
	err := view.Template.ExecuteTemplate(w, view.Layout, data)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	return err
}

// NewView to create a new View
func NewView(dir string, layout string, filenames ...string) (*View, error) {
	var files []string

	for _, file := range filenames {
		filePath := fmt.Sprintf("%s/%s", dir, file)
		files = append(files, filePath)
	}

	files = append(
		files,
		layoutFiles(dir)...,
	)

	t, err := template.New(layout).Funcs(funcMap).ParseFiles(files...)

	if err != nil {
		return nil, err
	}

	return &View{
		Template: t,
		Dir:      dir,
		Layout:   layout,
	}, nil

}

// layoutFiles returns a slice of strings representing
// the files used in the application
func layoutFiles(dir string) []string {
	dir = dir + LayoutDir
	files, err := filepath.Glob(dir + "*" + TemplateExt)

	if err != nil {
		panic(err)
	}

	return files
}
