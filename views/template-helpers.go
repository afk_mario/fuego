package views

import (
	"fmt"
	"github.com/gomarkdown/markdown"
	"github.com/microcosm-cc/bluemonday"
	"html/template"
	"time"
)

var safe = bluemonday.UGCPolicy()
var strip = bluemonday.StrictPolicy()

func toMarkDown(args ...interface{}) template.HTML {
	md := []byte(fmt.Sprintf("%s", args...))
	output := markdown.ToHTML(md, nil, nil)
	return template.HTML(output)
}

func toSafe(args ...interface{}) template.HTML {
	output := []byte(fmt.Sprintf("%s", args...))
	return template.HTML(safe.SanitizeBytes(output))
}
func toStrip(args ...interface{}) template.HTML {
	output := []byte(fmt.Sprintf("%s", args...))
	return template.HTML(strip.SanitizeBytes(output))
}

func date(fmt string, date interface{}) string {
	return dateInZone(fmt, date, "Local")
}

func dateInZone(fmt string, date interface{}, zone string) string {
	var t time.Time
	switch date := date.(type) {
	default:
		t = time.Now()
	case time.Time:
		t = date
	case int64:
		t = time.Unix(date, 0)
	case int:
		t = time.Unix(int64(date), 0)
	case int32:
		t = time.Unix(int64(date), 0)
	}

	loc, err := time.LoadLocation(zone)
	if err != nil {
		loc, _ = time.LoadLocation("UTC")
	}

	return t.In(loc).Format(fmt)
}

func toDate(str string) time.Time {
	t, _ := time.Parse(time.RFC1123Z, str)
	return t
}

var funcMap = template.FuncMap{
	"md":     toMarkDown,
	"safe":   toSafe,
	"strip":  toStrip,
	"toDate": toDate,
	"date":   date,
}
