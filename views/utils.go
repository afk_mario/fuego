package views

import (
	"net/http"
	"net/url"
	"strings"

	"fuego/logger"
)

// RedirectToIndex utility to redirct to the index route
func RedirectToIndex(writer http.ResponseWriter, request *http.Request) {
	http.Redirect(writer, request, "/", http.StatusSeeOther)
}

// RedirectToError Convenience function to redirect to the error message page
func RedirectToError(writer http.ResponseWriter, request *http.Request, msg string) {
	nURL := []string{"/err?msg=", msg}
	http.Redirect(writer, request, strings.Join(nURL, ""), 302)
}

// RedirectToNotFound Convenience function to redirect to the not found page
func RedirectToNotFound(writer http.ResponseWriter, request *http.Request, path string) {
	encodedPath := url.PathEscape(path)
	nURL := []string{"/404?url=", encodedPath}
	http.Redirect(writer, request, strings.Join(nURL, ""), 302)
}

// Must utility for error handling on view render
func Must(err error) {
	if err != nil {
		logger.Error(`[VIEWS] Failed to render view: %v`, err)
	}
}
