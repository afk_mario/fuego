package httpd

import (
	"net/http"
	"time"

	"fuego/config"
	"fuego/logger"
	"fuego/ui"

	"github.com/gorilla/mux"
)

// Serve starts a new HTTP server.
func Serve() *http.Server {
	listenAddr := config.Opts.ListenAddr()
	server := &http.Server{
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		IdleTimeout:  60 * time.Second,
		Handler:      setupHandler(),
	}

	server.Addr = listenAddr
	startHTTPServer(server)

	return server
}

func startHTTPServer(server *http.Server) {
	logger.Info(`[HTTPD] Listening on %q`, server.Addr)
	if err := server.ListenAndServe(); err != http.ErrServerClosed {
		logger.Fatal(`[HTTPD] Server failed to start: %v`, err)
	}
}

func setupHandler() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/healthcheck", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("OK"))
	}).Name("healthcheck")

	ui.Serve(router)

	return router
}
